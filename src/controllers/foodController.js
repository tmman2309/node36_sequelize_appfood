import sequelize from "../models/connect.js";
import initModels from "../models/init-models.js";

const model = initModels(sequelize);

// !Like API
// like
const like = async (req, res) => {
  const { user_id, res_id } = req.body;
  const likeResInfo = await model.like_res.findOne({
    where: {
      user_id,
      res_id,
    },
  });

  const userInfo = await model.users.findOne({
    where: {
      user_id,
    },
  });

  const resInfo = await model.restaurant.findOne({
    where: {
      res_id,
    },
  });

  if (likeResInfo) {
    res.send("Bạn đã like nhà hàng này rồi!");
  } else if (!userInfo || !resInfo) {
    res.send("Không tìm thấy dữ liệu!");
  } else {
    let newLike = {
      user_id,
      res_id,
      date_like: new Date().toISOString(),
    };

    await model.like_res.create(newLike);
    res.status(201).send("Like thành công!");
  }
};

// unlike
const unlike = async (req, res) => {
  const { user_id, res_id } = req.body;
  const likeResInfo = await model.like_res.findOne({
    where: {
      user_id,
      res_id,
    },
  });

  const userInfo = await model.users.findOne({
    where: {
      user_id,
    },
  });

  const resInfo = await model.restaurant.findOne({
    where: {
      res_id,
    },
  });
  if (!userInfo || !resInfo) {
    res.send("Không tìm thấy dữ liệu!");
  } else if (!likeResInfo) {
    res.send("Bạn chưa like nhà hàng này!");
  } else {
    await model.like_res.destroy({
      where: {
        user_id,
        res_id,
      },
    });
    res.status(201).send("Unlike thành công!");
  }
};

// getListLikeByRes
const getListLikeByRes = async (req, res) => {
  const { res_id } = req.params;
  const likeResInfo = await model.like_res.findAll({
    where: {
      res_id,
    },
  });

  if (likeResInfo.length) {
    res.status(200).send(likeResInfo);
  } else {
    res.send("Không tìm thấy dữ liệu!");
  }
};

// getListLikeByUser
const getListLikeByUser = async (req, res) => {
  const { user_id } = req.params;
  const likeResInfo = await model.like_res.findAll({
    where: {
      user_id,
    },
  });

  if (likeResInfo.length) {
    res.status(200).send(likeResInfo);
  } else {
    res.send("Không tìm thấy dữ liệu!");
  }
};

// !Rate API
// rate
const rate = async (req, res) => {
  const { user_id, res_id, amount } = req.body;
  const rateResInfo = await model.rate_res.findOne({
    where: {
      user_id,
      res_id,
    },
  });

  const userInfo = await model.users.findOne({
    where: {
      user_id,
    },
  });

  const resInfo = await model.restaurant.findOne({
    where: {
      res_id,
    },
  });

  if (rateResInfo) {
    res.send("Bạn đã đánh giá nhà hàng này rồi!");
  } else if (!userInfo || !resInfo) {
    res.send("Không tìm thấy dữ liệu!");
  } else if (typeof amount !== "number" || amount < 0 || amount > 5) {
    res.send("Rate không hợp lệ!");
  } else {
    let newRate = {
      user_id,
      res_id,
      amount,
      date_rate: new Date().toISOString(),
    };

    await model.rate_res.create(newRate);
    res.status(201).send("Đánh giá thành công!");
  }
};

// getListRateByRes
const getListRateByRes = async (req, res) => {
  const { res_id } = req.params;
  const rateResInfo = await model.rate_res.findAll({
    where: {
      res_id,
    },
  });

  if (rateResInfo.length) {
    res.status(200).send(rateResInfo);
  } else {
    res.send("Không tìm thấy dữ liệu!");
  }
};

// getListRateByUser
const getListRateByUser = async (req, res) => {
  const { user_id } = req.params;
  const rateResInfo = await model.rate_res.findAll({
    where: {
      user_id,
    },
  });

  if (rateResInfo.length) {
    res.status(200).send(rateResInfo);
  } else {
    res.send("Không tìm thấy dữ liệu!");
  }
};

// !Order API
const order = async (req, res) => {
  const { user_id, food_id, amount, arr_sub_id } = req.body;
  const userInfo = await model.users.findOne({
    where: {
      user_id,
    },
  });

  const foodInfo = await model.food.findOne({
    where: {
      food_id,
    },
  });

  const subFoodInfo = await model.sub_food.findAll({
    where: {
      food_id,
    },
  });

  if (!userInfo || !foodInfo) {
    res.send("Không tìm thấy dữ liệu!");
  } else if (typeof amount !== "number" || amount < 0) {
    res.send("Amount không hợp lệ!");
  } else {
    const subFoodIds = subFoodInfo.map((subFood) => String(subFood.sub_id));
    const isValidSubIds = subFoodIds.includes(arr_sub_id);

    if (!isValidSubIds) {
      res.send("Món phụ không thuộc món chính!");
    } else {
      let newOrder = {
        user_id,
        food_id,
        amount,
        arr_sub_id,
        code: `ID${new Date().getTime()}`,
      };
      await model.orders.create(newOrder);
      res.status(201).send("Order thành công!");
    }
  }
};

export {
  getListLikeByRes,
  getListLikeByUser,
  getListRateByRes,
  getListRateByUser,
  like,
  order,
  rate,
  unlike,
};
