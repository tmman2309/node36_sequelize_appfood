import { Sequelize } from "sequelize";
import config from "../config/config.js";

const {
  databaseName,
  databaseUserName,
  databasePassword,
  databasePort,
  databaseHost,
} = config;

const sequelize = new Sequelize(
  databaseName,
  databaseUserName,
  databasePassword,
  {
    port: databasePort,
    dialect: "mysql",
    host: databaseHost,
  }
);

export default sequelize;