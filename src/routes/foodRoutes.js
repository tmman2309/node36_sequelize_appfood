import express from "express";
import {
  getListLikeByRes,
  getListLikeByUser,
  getListRateByRes,
  getListRateByUser,
  like,
  order,
  rate,
  unlike,
} from "../controllers/foodController.js";

const foodRoutes = express.Router();

// Like API
foodRoutes.post("/like", like);
foodRoutes.post("/unlike", unlike);
foodRoutes.get("/like-of-res/:res_id", getListLikeByRes);
foodRoutes.get("/like-of-user/:user_id", getListLikeByUser);

// Rate API
foodRoutes.post("/rate", rate);
foodRoutes.get("/rate-of-res/:res_id", getListRateByRes);
foodRoutes.get("/rate-of-user/:user_id", getListRateByUser);

// Order API
foodRoutes.post("/order", order);

export default foodRoutes;
